#!/bin/bash

. ./path.sh || exit 1;

# basic settings
stage=-1       # stage to start
stop_stage=100 # stage to stop
verbose=1      # verbosity level (lower is less info)
n_gpus=3       # number of gpus in training

# directory path setting
download_dir=downloads # direcotry to save downloaded files
dumpdir=dump           # directory to dump features

# training related setting
tag=""     # tag for directory to save model
resume=""  # checkpoint path to resume training
           # (e.g. <path>/<to>/checkpoint-10000steps.pkl)
generator=usfgan # available values: usfgan, parallel_hn_usfgan, cascade_hn_usfgan
discriminator=pwg # available values: hifigan, pwg, univnet
train=usfgan # available values:usfgan, hn_usfgan

# decoding related setting
checkpoint="" # checkpoint path to be used for decoding
              # if not provided, the latest one will be used
              # (e.g. <path>/<to>/checkpoint-400000steps.pkl)

# shellcheck disable=SC1091
. utils/parse_options.sh || exit 1;

train_set="train_nodev" # name of training data directory
dev_set="dev"           # name of development data direcotry
eval_set="eval"         # name of evaluation data direcotry

set -euo pipefail


# stage -1: Data download
if [ "${stage}" -le -1 ] && [ "${stop_stage}" -ge -1 ]; then
    echo "Stage -1: Data download"
    local/data_download.sh "${download_dir}"
fi


# stage 0: Data preparation
if [ "${stage}" -le 0 ] && [ "${stop_stage}" -ge 0 ]; then
    echo "Stage 0: Data preparation"
    local/data_prep.sh \
        --train_set "${train_set}" \
        --dev_set "${dev_set}" \
        --eval_set "${eval_set}" \
        "${download_dir}" data
fi

# stage 1: Extract features
if [ "${stage}" -le 1 ] && [ "${stop_stage}" -ge 1 ]; then
    echo "Stage 1: Extract features"
    usfgan-extract-features audio=data/${train_set}/wav.scp || exit 1;
    usfgan-extract-features audio=data/${dev_set}/wav.scp || exit 1;
    usfgan-extract-features audio=data/${eval_set}/wav.scp || exit 1;
    usfgan-compute-statistics feats=data/${train_set}/wav.list stats=data/${train_set}/wav.joblib || exit 1;
fi

# setup the experiment directory
if [ -z "${tag}" ]; then
    exp_dir="exp/${train_set}_arctic_${generator}"
else
    exp_dir="exp/${train_set}_arctic_${tag}"
fi


# stage 2: Training
if [ "${stage}" -le 2 ] && [ "${stop_stage}" -ge 2 ]; then
    echo "Stage 2: Training"
    usfgan-train generator=${generator} discriminator=${discriminator} train=${train} data=arctic-24k out_dir=${exp_dir} || exit 1;
fi

# stage 3: Decode
if [ "${stage}" -le 3 ] && [ "${stop_stage}" -ge 3 ]; then
    echo "Stage 3: Decode"
    # pick last
    [ -z "${checkpoint}" ] && checkpoint="$(ls -dt "${exp_dir}"/checkpoints/*.pkl | head -1 || true)"
    out_dir="${exp_dir}/wav/$(basename "${checkpoint}" .pkl)"
    # decode the dev set
    # usfgan-decode generator=${generator} out_dir=${out_dir}/${dev_set} checkpoint_path=${checkpoint} data=arctic-24k data.eval_feat=data/${dev_set}/wav.list
    # decode the eval set 
    usfgan-decode generator=${generator} out_dir=${out_dir}/${eval_set}-12st checkpoint_path=${checkpoint} data=arctic-24k data.eval_feat=data/${eval_set}/wav.list f0_factor=2.0
    usfgan-decode generator=${generator} out_dir=${out_dir}/${eval_set}-8st checkpoint_path=${checkpoint} data=arctic-24k data.eval_feat=data/${eval_set}/wav.list f0_factor=1.58740
    usfgan-decode generator=${generator} out_dir=${out_dir}/${eval_set}-4st checkpoint_path=${checkpoint} data=arctic-24k data.eval_feat=data/${eval_set}/wav.list f0_factor=1.25992
    usfgan-decode generator=${generator} out_dir=${out_dir}/${eval_set}-0st checkpoint_path=${checkpoint} data=arctic-24k data.eval_feat=data/${eval_set}/wav.list f0_factor=1.0
    usfgan-decode generator=${generator} out_dir=${out_dir}/${eval_set}--4st checkpoint_path=${checkpoint} data=arctic-24k data.eval_feat=data/${eval_set}/wav.list f0_factor=0.79370
    usfgan-decode generator=${generator} out_dir=${out_dir}/${eval_set}--8st checkpoint_path=${checkpoint} data=arctic-24k data.eval_feat=data/${eval_set}/wav.list f0_factor=0.62996
    usfgan-decode generator=${generator} out_dir=${out_dir}/${eval_set}--12st checkpoint_path=${checkpoint} data=arctic-24k data.eval_feat=data/${eval_set}/wav.list f0_factor=0.5
fi
echo "Finished."