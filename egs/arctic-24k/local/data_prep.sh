#!/bin/bash

# Copyright 2019 Tomoki Hayashi
#  MIT License (https://opensource.org/licenses/MIT)

# shellcheck disable=SC1091
. ./path.sh || exit 1;

fs=24000
num_dev=100
num_eval=100
train_set="train_nodev"
dev_set="dev"
eval_set="eval"
shuffle=true

# shellcheck disable=SC1091
. utils/parse_options.sh || exit 1;

db_root=$1
data_dir=$2

# check arguments
if [ $# != 2 ]; then
    echo "Usage: $0 <download_dir> <data_dir>"
    echo "e.g.: $0 downloads data"
    echo ""
    echo "Options:"
    echo "    --num_dev: number of development uttreances (default=250)."
    echo "    --num_eval: number of evaluation uttreances (default=250)."
    echo "    --train_set: name of train set (default=train_nodev)."
    echo "    --dev_set: name of dev set (default=dev)."
    echo "    --eval_set: name of eval set (default=eval)."
    echo "    --shuffle: whether to perform shuffle in making dev / eval set (default=false)."
    exit 1
fi

set -euo pipefail


[ ! -e "${data_dir}/all" ] && mkdir -p "${data_dir}/all"

# set filenames
scp="${data_dir}/all/wav.scp"

# check file existence
[ -e "${scp}" ] && rm "${scp}"

# make the output_dir
mkdir -p ${db_root}/24k

# make scp
find ${db_root}/cmu_us_* -follow -name "*.wav" | sort -k1.30,1.35 | while read -r wav; do
    spk="$(echo ${wav} | sed -E -e "s/.*cmu_us_(...)_arctic.*$/\1/g")"
    id="${spk}_$(basename "${wav}" | sed -e "s/\.[^\.]*$//g")"
    # sox command explain
    # "-t wav -" read the wav from stdin 
    # "-c 1 -b 16 -t wav -" specify the output format and output to stdout
    # "remix 1 rate ${fs}" select the left channel and changed the sampling rate.
    cat ${wav} | sox -t wav - -c 1 -b 16 -t wav - remix 1 rate ${fs} > ${db_root}/24k/${id}.wav
    echo "${db_root}/24k/${id}.wav" >> "${scp}"
done
# split
num_all=$(wc -l < "${scp}")
num_deveval=$((num_dev + num_eval))
num_train=$((num_all - num_deveval))
utils/split_data.sh \
    --num_first "${num_train}" \
    --num_second "${num_deveval}" \
    --shuffle "${shuffle}" \
    "${data_dir}/all" \
    "${data_dir}/${train_set}" \
    "${data_dir}/deveval"
utils/split_data.sh \
    --num_first "${num_dev}" \
    --num_second "${num_eval}" \
    --shuffle "${shuffle}" \
    "${data_dir}/deveval" \
    "${data_dir}/${dev_set}" \
    "${data_dir}/${eval_set}"

# remove tmp directories
rm -rf "${data_dir}/all"
rm -rf "${data_dir}/deveval"

echo "Successfully prepared data."
